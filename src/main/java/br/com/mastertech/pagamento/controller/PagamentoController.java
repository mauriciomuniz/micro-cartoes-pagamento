package br.com.mastertech.pagamento.controller;

import br.com.mastertech.pagamento.DTO.PagamentoDTO;
import br.com.mastertech.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoDTO efetuarPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO) {
            PagamentoDTO pagamentoRealizado = pagamentoService.efetuarPagamento(pagamentoDTO);
            return pagamentoRealizado;
    }

    @GetMapping("/pagamentos/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoDTO> buscarPagamentoPorId(@PathVariable (name = "id_cartao") int idCartao) {
            List<PagamentoDTO> pagamentos = pagamentoService.buscarPagamentoPorIdCartao(idCartao);
            return pagamentos;
    }

    @DeleteMapping("/pagamento/{id_cartao}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void deletarRegistros(@PathVariable (name = "id_cartao") int idCartao) {
        pagamentoService.deletarPagamentoPeloIdCartao(idCartao);
    }
}
