package br.com.mastertech.pagamento.card;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CardPaymentDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 400) {
            return new PaymentNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }

}
