package br.com.mastertech.pagamento.card;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CardPaymentConfiguration {

    @Bean
    public ErrorDecoder getCardPaymentDecoder() {
        return new CardPaymentDecoder();
    }
}
