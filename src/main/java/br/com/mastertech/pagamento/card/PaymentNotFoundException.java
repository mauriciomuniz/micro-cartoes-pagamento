package br.com.mastertech.pagamento.card;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Não existem registros para esse cartão")
public class PaymentNotFoundException extends RuntimeException {
}
