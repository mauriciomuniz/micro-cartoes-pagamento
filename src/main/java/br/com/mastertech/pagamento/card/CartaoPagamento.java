package br.com.mastertech.pagamento.card;

import br.com.mastertech.pagamento.DTO.CartaoGetDTO;
import br.com.mastertech.pagamento.model.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "CARTAO", configuration = CardPaymentConfiguration.class)
public interface CartaoPagamento {

    @GetMapping("/cartao/id/{id_cartao}")
    Cartao buscarCartaoPorID(@PathVariable(name = "id_cartao") int numero);
}