package br.com.mastertech.pagamento.card;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão inativo")
public class InactiveCardException extends RuntimeException{
}