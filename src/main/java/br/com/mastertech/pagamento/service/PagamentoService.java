package br.com.mastertech.pagamento.service;

import br.com.mastertech.pagamento.DTO.PagamentoDTO;
import br.com.mastertech.pagamento.card.PaymentNotFoundException;
import br.com.mastertech.pagamento.card.CartaoPagamento;
import br.com.mastertech.pagamento.card.InactiveCardException;
import br.com.mastertech.pagamento.model.Cartao;
import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoPagamento cartaoPagamento;

    public PagamentoDTO efetuarPagamento(PagamentoDTO pagamentoDTO) {
        Pagamento pagamento = new Pagamento();
        Cartao cartao = buscarCartao(pagamentoDTO.getCartao_id());

        pagamento.setCartao(cartao);
        pagamento.setDescricao(pagamentoDTO.getDescricao());
        pagamento.setValor(pagamentoDTO.getValor());
        Pagamento pagamentoSalvo = salvarPagamento(pagamento);

        return montaSaidaPagamento(pagamentoSalvo, cartao);
    }

    public List<PagamentoDTO> buscarPagamentoPorIdCartao(int idCartao) {
        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(idCartao);

        if (!pagamentos.isEmpty()) {
            return montaSaidaPagamento(pagamentos);
        } else {
            throw new PaymentNotFoundException();
        }

    }

    public PagamentoDTO montaSaidaPagamento(Pagamento pagamento, Cartao cartao) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO();
        pagamentoDTO.setCartao_id(cartao.getId());
        pagamentoDTO.setDescricao(pagamento.getDescricao());
        pagamentoDTO.setValor(pagamento.getValor());
        pagamentoDTO.setId(pagamento.getId());
        return pagamentoDTO;
    }

    public List<PagamentoDTO> montaSaidaPagamento(List<Pagamento> pagamentos) {
        List<PagamentoDTO> pagamentosDTO = new ArrayList<>();
        for (Pagamento aux : pagamentos) {
            PagamentoDTO pagamentoDTO = new PagamentoDTO();

            pagamentoDTO.setId(aux.getId());
            pagamentoDTO.setValor(aux.getValor());
            pagamentoDTO.setCartao_id(aux.getCartao().getId());
            pagamentoDTO.setDescricao(aux.getDescricao());
            pagamentosDTO.add(pagamentoDTO);
        }
        return pagamentosDTO;
    }

    public void deletarPagamentoPeloIdCartao(int idCartao) {
        pagamentoRepository.deleteByCartaoId(idCartao);
    }

    public Pagamento salvarPagamento(Pagamento pagamento) {
        Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);
        return pagamentoSalvo;
    }

    public Cartao buscarCartao(int numero) {
            Cartao cartao = cartaoPagamento.buscarCartaoPorID(numero);
            if (cartao.isAtivo()) {
                return cartao;
            } else {
                throw new InactiveCardException();
            }
    }
}