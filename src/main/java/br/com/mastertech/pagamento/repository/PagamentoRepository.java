package br.com.mastertech.pagamento.repository;

import br.com.mastertech.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    List<Pagamento> findAllByCartaoId(int idCartao);

    void deleteByCartaoId(int cartaoId);
}
